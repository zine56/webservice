
/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/camelcase */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';
import { GcloudAuthenticationInstance } from './cloud-storage/gcloudAuthenticationInstance';
import * as nock from 'nock'
import { HttpAdapterHost } from '@nestjs/core';
import { AllExceptionsFilter } from './../src/exception-filters/all-exceptions.filter';

const resultUpload = {
  name: 'aaaaa',
  timeCreated: 'aa'
};
const mockedBucket = {
  upload: jest.fn().mockImplementation(() => {
    return new Promise((res) => { res([{}, resultUpload]) });
  })
};
const mockedStorage = {
  bucket: jest.fn(() => mockedBucket)
};


jest.mock('@google-cloud/storage', () => {
  return {
    Storage: jest.fn(() => mockedStorage)
  };
});

describe('AppController (e2e)', () => {
  let app: INestApplication;
  const OLD_ENV = process.env;

  beforeEach(async () => {
    jest.resetModules();
    jest.restoreAllMocks();
    process.env = { ...OLD_ENV };
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    const { httpAdapter } = app.get(HttpAdapterHost);
    app.useGlobalPipes(new ValidationPipe());
    app.useGlobalFilters(new AllExceptionsFilter(httpAdapter));
    await app.init();
  });
  afterAll(() => {
    process.env = OLD_ENV;
  });
  afterEach(() => {
    jest.clearAllMocks();
    jest.restoreAllMocks();
  })

  it('/writeSKU - OK', async (done) => {
    nock(process.env.DESCRIPTION_URL).post('/getDescription', { products: ['Item'], sku: 'SKU' })
      .reply(200, {
        result: {
          ID: '1',
          brand: 'brand',
          description: 'desc',
          displayName: 'disp'
        }
      });
    jest
      .spyOn(GcloudAuthenticationInstance, 'createGcloudAuthenticationBucket')
      .mockReturnValueOnce({
        bucket: (name: any): any => { return name }, file: (): any => { return '' }, createWriteStream: (): string => { return '' },
      });


    const response = await request(app.getHttpServer())
      .post('/WriteSKU')
      .send({ products: ['Item'], sku: 'SKU' })

    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toEqual({ storage: resultUpload });
    done();
  });

  it('/writeProduct - OK', async (done) => {
    nock(process.env.DESCRIPTION_URL).post('/getDescription', { products: ['Item'], sku: 'SKU' })
      .reply(200, {
        result: {
          ID: '1',
          brand: 'brand',
          description: 'desc',
          displayName: 'disp'
        }
      });
    jest
      .spyOn(GcloudAuthenticationInstance, 'createGcloudAuthenticationBucket')
      .mockReturnValueOnce({
        bucket: (name: any): any => { return name }, file: (): any => { return '' }, createWriteStream: (): string => { return '' },
      });


    const response = await request(app.getHttpServer())
      .post('/WriteProduct')
      .send({ products: ['Item'], sku: 'SKU' })

    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toEqual({ storage: resultUpload });
    done();
  });

  it('/WriteDataSheet - OK', async (done) => {
    nock(process.env.DATASHEET_URL).post('/getDataSheet', { products: ['Item'], sku: 'SKU' })
      .reply(200, {
        result: {
          ID: '1',
          brand: 'brand',
          description: 'desc',
          displayName: 'disp'
        }
      });
    jest
      .spyOn(GcloudAuthenticationInstance, 'createGcloudAuthenticationBucket')
      .mockReturnValueOnce({
        bucket: (name: any): any => { return name }, file: (): any => { return '' }, createWriteStream: (): string => { return '' },
      });

    const response = await request(app.getHttpServer())
      .post('/WriteDataSheet')
      .send({ products: ['Item'], sku: 'SKU' })

    expect(response.status).toBe(200);
    expect(JSON.parse(response.text)).toEqual({ storage: resultUpload });
    done();
  });

  it('/test - 404', async (done) => {
    const response = await request(app.getHttpServer())
      .get('/test');

    expect(response.status).toBe(404);
    expect(response.body).toEqual({
      status: 404,
      statusDescription: '{\"statusCode\":404,\"message\":\"Cannot GET /test\",\"error\":\"Not Found\"}',
      url: '/test'
    })
    done();
  });

  it('Bad request - 400', async (done) => {
    const response = await request(app.getHttpServer())
      .post('/WriteDataSheet')
      .send();

    expect(response.status).toBe(400);
    expect(JSON.parse(response.text)).toEqual({
      status: 400,
      statusDescription: '{\"statusCode\":400,\"message\":[\"products must contain at least 1 elements\",\"products should not be empty\",\"products must be an array\",\"sku should not be empty\",\"sku must be a string\"],\"error\":\"Bad Request\"}',
      url: '/WriteDataSheet'
    });
    done();
  });


  it('Internal Error - 500', async (done) => {
    nock(process.env.DATASHEET_URL).filteringPath(function (path) {
      return '/';
    })
      .post("/")
      .reply(500);

    const response = await request(app.getHttpServer())
      .post('/WriteDataSheet')
      .send({ products: ['Item'], sku: 'SKU_PE' })

    expect(response.status).toBe(500);
    expect(JSON.parse(response.text)).toEqual({
      status: 500,
      statusDescription: '"Request failed with status code 500"',
      url: '/WriteDataSheet'
    });
    done();
  });

});
