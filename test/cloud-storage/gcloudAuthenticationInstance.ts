/* eslint-disable @typescript-eslint/no-unused-vars */
const GcloudAuthenticationInstance = {
  createGcloudAuthenticationBucket: () => {
    const storage = {
      bucket(name) {
        return this;
      },
      file(name) {
        return this;
      },
      createWriteStream(name) {
        return this;
      }
    };
    return storage;
  },
};

export { GcloudAuthenticationInstance };