import { Module, HttpModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { XlsxService } from './services/xlsx.service';
import { StorageService } from './services/storage.service';
import { LoggerService } from './services/logger.service';
import { DescriptionService } from './services/description.service';
import { DataSheetService } from './services/dataSheet.service';

@Module({
  imports: [HttpModule],
  controllers: [AppController],
  providers: [AppService, XlsxService, StorageService, LoggerService, DescriptionService, DataSheetService],
})
export class AppModule { }
