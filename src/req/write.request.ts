import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString, IsArray, ArrayMinSize } from 'class-validator';

export class WriteRequest {
    @IsArray()
    @IsNotEmpty()
    @ArrayMinSize(1)
    @ApiProperty({ example: ['Item-100698']})
    readonly products: Array<string>;

    @IsString()
    @IsNotEmpty()
    @ApiProperty({ example: 'SKU_PE' })
    readonly sku: string;

}


