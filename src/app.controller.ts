import { Controller, Post, Res, HttpStatus, Body } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { XlsxService } from './services/xlsx.service';
import { StorageService } from './services/storage.service';
import { Response } from 'express';
import { DescriptionService } from './services/description.service';
import { WriteRequest } from './req/write.request';
import { DataSheetService } from './services/dataSheet.service';
import { LoggerService } from './services/logger.service';

@ApiTags('fileCreation')
@Controller()
export class AppController {
  constructor(
    private xlsxService: XlsxService,
    private storageService: StorageService,
    private descriptionService: DescriptionService,
    private dataSheetService: DataSheetService,
    private loggerService: LoggerService
  ) { }

  @Post('WriteSKU')
  async writeSKU(@Body() request: WriteRequest, @Res() res: Response): Promise<Response> {
    this.loggerService.customInfo(null, 'SKU - Starting...');
    const descriptionData = await this.descriptionService.getDescription(request.products, request.sku);
    const header = [['/atg/commerce/catalog/SecureProductCatalog:sku', '', '', '', 'LOCALE=en_US']];
    const fileContext = 'PLANTILLA-DESCRIPCION-NIVEL SKU';
    const fileName = await this.xlsxService.writeFile(fileContext, header, descriptionData);
    const storage = await this.storageService.uploadFile(fileName);
    this.loggerService.customInfo(null, 'SKU - file created and uploaded: ' + fileName);
    return res.status(HttpStatus.OK).json({ storage });
  }

  @Post('WriteProduct')
  async writeProduct(@Body() request: WriteRequest, @Res() res: Response): Promise<Response> {
    this.loggerService.customInfo(null, 'PRODUCT - Starting...');
    const descriptionData = await this.descriptionService.getDescription(request.products, request.sku);
    const header = [['/atg/commerce/catalog/SecureProductCatalog:product', '', '', '', 'LOCALE=en_US']];
    const fileContext = 'PLANTILLA-DESCRIPCION-NIVEL PRODUCT';
    const fileName = await this.xlsxService.writeFile(fileContext, header, descriptionData);
    const storage = await this.storageService.uploadFile(fileName);
    this.loggerService.customInfo(null, 'PRODUCT - file created and uploaded: ' + fileName);
    return res.status(HttpStatus.OK).json({ storage });
  }

  @Post('WriteDataSheet')
  async writeDataSheet(@Body() request: WriteRequest, @Res() res: Response): Promise<Response> {
    this.loggerService.customInfo(null, 'DATASHEET - Starting...');
    const dataSheetData = await this.dataSheetService.getDataSheet(request.products, request.sku);
    const header = [['/atg/commerce/catalog/SecureProductCatalog:product', 'FORMAT=itemized', '', '', 'LOCALE=en_US']];
    const fileContext = 'PLANTILLA FICHA TECNICA';
    const fileName = await this.xlsxService.writeFile(fileContext, header, dataSheetData);
    const storage = await this.storageService.uploadFile(fileName);
    this.loggerService.customInfo(null, 'DATASHEET - file created and uploaded: ' + fileName);
    return res.status(HttpStatus.OK).json({ storage });
  }


}
