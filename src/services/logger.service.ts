import { Injectable, Logger } from '@nestjs/common';
import { stringify } from 'circular-json';
import { info, error } from 'npmlog';
import * as moment from 'moment-timezone';

@Injectable()
export class LoggerService extends Logger {

    private serviceReference = process.env.SERVICE_REF;
    private nodeReference = process.env.NODE_REF;
    private environment = process.env.ENVIRONMENT;

    async customInfo(headers: Record<string, any>, message: string) {
        const dateFormat = moment().tz("America/Santiago").format('YYYY-MM-DD HH:mm:ss.SSS');
        const level = 'info';
        info(dateFormat, level, stringify(message), this.buildMessage(headers));
    }
    async customError(headers: Record<string, any>, message: string) {
        const dateFormat = moment().tz("America/Santiago").format('YYYY-MM-DD HH:mm:ss.SSS');
        const level = 'error';
        error(dateFormat, level, stringify(message), this.buildMessage(headers));
    }

    private buildMessage(headers: Record<string, any>): string {

        return "";
    }


}
