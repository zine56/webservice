import { Injectable, HttpService } from "@nestjs/common";

///////////////////

import * as common from 'common'

import { UpdateRequest } from 'common/build/models/requests/types/update.requests'

////////////////////

@Injectable()
export class DescriptionService {
    constructor(private httpService: HttpService) { 
        //no problem with this
        common.default.services('qa','key','secret')
        //but uncomment this line and everything goes to hell
        //new UpdateRequest('','','')
    }

    async getDescription(products: Array<string>, sku: string): Promise<Array<Record<string, any>>> {
        const response = await this.httpService.axiosRef({
            method: 'post',
            baseURL: process.env.DESCRIPTION_URL,
            url: '/getDescription',
            data: {
                products,
                sku
            }
        });
        return response.data.result;
    }
}