import { Injectable } from '@nestjs/common';
import * as path from 'path';
import * as fs from 'fs';
import { Storage, UploadResponse } from '@google-cloud/storage';
import * as moment from 'moment-timezone';

@Injectable()
export class StorageService {
    constructor() {
        const base64Credential = process.env.GCLOUD_CREDENTIAL;
        //const buffer = Buffer.from(base64Credential, 'base64');
        //const credentialDecode = buffer.toString();
        this.credentials = { };
        /*this.storage = new Storage({
            projectId: this.projectId,
            credentials: this.credentials
        })*/
    }

    private projectId: string = process.env.GCLOUD_PROJECT_ID;
    private bucketName: string = process.env.GCLOUD_BUCKET_NAME;
    private credentials: Record<string, any>;

    private storage: Storage;

    async uploadFile(fileName: string): Promise<Record<string, any>> {
        const destination = moment().tz("America/Santiago").format('DD-MM-YYYY') + '/' + fileName;
        const file = path.join(__dirname, '..', '..', 'files', fileName);
        const data: UploadResponse = await this.storage.bucket(this.bucketName).upload(file, {
            destination
        });
        await this.deleteLocalFile(fileName);
        return { name: data[1].name, timeCreated: data[1].timeCreated };
    }

    async deleteLocalFile(fileName: string): Promise<any> {
        fs.unlinkSync(path.join(__dirname, '..', '..', 'files', fileName));
    }
}
