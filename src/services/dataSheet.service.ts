import { Injectable, HttpService } from "@nestjs/common";

@Injectable()
export class DataSheetService {
    constructor(private httpService: HttpService) { }

    async getDataSheet(products: Array<string>, sku: string): Promise<Array<Record<string, any>>> {
        const response = await this.httpService.axiosRef({
            method: 'post',
            baseURL: process.env.DATASHEET_URL,
            url: '/getDataSheet',
            data: {
                products,
                sku
            }
        });
        return response.data.result;
    }
}