import { Injectable } from '@nestjs/common';
import { utils, WorkBook, writeFile, WorkSheet } from 'xlsx';
import * as path from 'path';
import * as fs from 'fs';
import * as moment from 'moment-timezone';

@Injectable()
export class XlsxService {

    async writeFile(fileContext: string, header: Array<Array<string>>, data: Array<Record<string, any>>): Promise<string> {
        const location: string = path.join(__dirname, '..', '..', 'files');
        const wb: WorkBook = utils.book_new();

        const fileName = fileContext + '-' + moment().tz("America/Santiago").format('HH.mm') + '.xls';
        const ws: WorkSheet = utils.aoa_to_sheet(header);
        utils.sheet_add_json(ws, data, { origin: "A2" });
        utils.book_append_sheet(wb, ws, 'Product');

        if (!fs.existsSync(location)) {
            fs.mkdirSync(location, { recursive: true });
        }
        await writeFile(wb, path.join(location, fileName));

        return fileName;
    }
}
