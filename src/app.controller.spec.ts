/* eslint-disable @typescript-eslint/no-empty-function */
/* eslint-disable @typescript-eslint/camelcase */
/* eslint-disable @typescript-eslint/no-unused-vars */
import { Test, TestingModule } from '@nestjs/testing';
import { AppController } from './app.controller';
import { StorageService } from './services/storage.service';
import { XlsxService } from './services/xlsx.service';
import { DescriptionService } from './services/description.service';
import { HttpModule, BadRequestException, NotFoundException, InternalServerErrorException, INestApplication, HttpException } from '@nestjs/common';
import { DataSheetService } from './services/dataSheet.service';
import { LoggerService } from './services/logger.service';
import { AllExceptionsFilter } from './exception-filters/all-exceptions.filter';
import { APP_FILTER, HttpAdapterHost } from '@nestjs/core';
import * as nock from 'nock'
import * as npmlog from 'npmlog';
import * as fs from 'fs';
import * as xlsx from 'xlsx';
import * as request from 'supertest';

const mockJson = jest.fn();
const mockStatus = jest.fn().mockImplementation(() => ({
  json: mockJson
}));
const mockGetRequest = jest.fn().mockImplementation(() => ({
  headers: mockJson,
  url: 'test'
}));
const mockGetResponse = jest.fn().mockImplementation(() => ({
  status: mockStatus
}));
const mockHttpArgumentsHost = jest.fn().mockImplementation(() => ({
  getResponse: mockGetResponse,
  getRequest: mockGetRequest
}));
const mockArgumentsHost = {
  switchToHttp: mockHttpArgumentsHost,
  getArgByIndex: jest.fn(),
  getArgs: jest.fn(),
  getType: jest.fn(),
  switchToRpc: jest.fn(),
  switchToWs: jest.fn()
};

const resultUpload = {
  name: 'aaaaa',
  timeCreated: 'aa'
};
const mockedBucket = {
  upload: jest.fn().mockImplementation(() => {
    return new Promise((res) => { res([{}, resultUpload]) });
  })
};
const mockedStorage = {
  bucket: jest.fn(() => mockedBucket)
};
const mockedResponse: any = {
  status: (status: any) => {
    return {
      status: status, json: (json: any) => {
        return json
      }
    }
  },
};

const requestData = {
  products: ['Item'],
  sku: 'SKU'
};
const xlsxData = {
  header: [['h']],
  fileContext: 'file',
  data: [{
    ID: '1',
    brand: 'br',
    description: 'de',
    displayName: 'di'
  }]
};
let logResult = {};
const OLD_ENV = process.env;

jest.mock('@google-cloud/storage', () => {
  return {
    Storage: jest.fn(() => mockedStorage)
  };
});
jest.mock('npmlog', () => {
  return {
    error: jest.fn((date, level, message, headers) => { logResult = { date, level, message, headers } }),
    info: jest.fn((date, level, message, headers) => { logResult = { date, level, message, headers } })
  };
});
jest.mock('fs');
jest.mock('xlsx');

describe('test 1', () => {
  let appController: AppController;
  let descriptionService: DescriptionService;
  let xlsxService: XlsxService;
  let storageService: StorageService;
  let dataSheetService: DataSheetService;
  let loggerService: LoggerService;
  let allExceptions: AllExceptionsFilter;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [AppController],
      providers: [StorageService, DescriptionService, XlsxService, DataSheetService, LoggerService, AllExceptionsFilter, {
        provide: APP_FILTER,
        useClass: AllExceptionsFilter,
      },]
    }).compile();

    appController = app.get<AppController>(AppController);
    storageService = app.get<StorageService>(StorageService);
    descriptionService = app.get<DescriptionService>(DescriptionService);
    dataSheetService = app.get<DataSheetService>(DataSheetService);
    xlsxService = app.get<XlsxService>(XlsxService);
    loggerService = app.get<LoggerService>(LoggerService);
    allExceptions = app.get<AllExceptionsFilter>(AllExceptionsFilter);
  });

  it('Services', async (done) => {
    const expectedData = {
      ID: '1',
      brand: 'brand',
      description: 'desc',
      displayName: 'disp'
    };

    nock(/.*/).post('/getDescription', { products: requestData.products, sku: requestData.sku })
      .reply(200, {
        result: {
          ID: '1',
          brand: 'brand',
          description: 'desc',
          displayName: 'disp'
        }
      });
    const dataDescription = await descriptionService.getDescription(requestData.products, requestData.sku);
    expect(dataDescription).toEqual(expectedData);

    nock(/.*/).post('/getDataSheet', { products: requestData.products, sku: requestData.sku })
      .reply(200, {
        result: {
          ID: '1',
          brand: 'brand',
          description: 'desc',
          displayName: 'disp'
        }
      });
    const dataSheet = await dataSheetService.getDataSheet(requestData.products, requestData.sku);
    expect(dataSheet).toEqual(expectedData);
    jest.spyOn(fs, 'mkdirSync').mockImplementation(() => {
      return 'files';
    });
    const file = await xlsxService.writeFile(xlsxData.fileContext, xlsxData.header, dataDescription);
    expect(typeof file).toBe('string');
    jest.spyOn(fs, 'unlinkSync').mockImplementation(() => {
      jest.fn();
    });
    const result = await storageService.uploadFile(file);
    expect(result).toEqual(resultUpload);
    done();
  });

  it('Controllers', async (done) => {
    const expected: Record<string, any> = {
      storage: {
        name: 'test',
        timeCreated: 'created'
      }
    };

    jest.spyOn(descriptionService, 'getDescription').mockImplementation(async () => {
      return new Promise((res) => res([{}]))
    });

    jest.spyOn(dataSheetService, 'getDataSheet').mockImplementation(async () => {
      return new Promise((res) => res([{}]))
    });

    jest.spyOn(xlsxService, 'writeFile').mockImplementation(async () => {
      return new Promise((res) => res('file'))
    });

    jest.spyOn(storageService, 'uploadFile').mockImplementation(async () => {
      return new Promise((res) => res(
        { name: 'test', timeCreated: 'created' }
      ))
    });
    const writeSKU = await appController.writeSKU(requestData, mockedResponse);
    expect(writeSKU).toEqual(expected);

    const writeProduct = await appController.writeProduct(requestData, mockedResponse);
    expect(writeProduct).toEqual(expected);

    const writeDataSheet = await appController.writeDataSheet(requestData, mockedResponse);
    expect(writeDataSheet).toEqual(expected);
    done();
  });

  it('Logger', async (done) => {
    const expectedError = {
      level: 'error',
      message: "\"error\""
    };
    const headers: Record<string, string> = {
      'x-txref': '1',
      'x-cmref': '2',
      'x-rhsref': '3',
      'x-chref': '4',
      'x-prref': '5',
      'x-commerce': '6',
      'x-country': '7',
      'x-usrtx': '8'
    }
    loggerService.customError(headers, 'error')
    expect(logResult).toMatchObject(expectedError);
    const expectedInfo = {
      level: 'info',
      message: "\"info\""
    }
    loggerService.customInfo(null, 'info')
    expect(logResult).toMatchObject(expectedInfo);
    done();
  });

  it('Exceptions', (done) => {
    const badRequestExpected = {
      status: 400,
      statusDescription: JSON.stringify({
        statusCode: 400,
        message: 'Bad Request'
      }),
      url: 'test'
    };
    allExceptions.catch(
      new BadRequestException,
      mockArgumentsHost
    );
    expect(mockJson).toBeCalledWith(badRequestExpected);
    mockJson.mockReset();

    const notFoundExpected = {
      status: 404,
      statusDescription: JSON.stringify({
        statusCode: 404,
        message: 'Not Found'
      }),
      url: 'test'
    };
    allExceptions.catch(
      new NotFoundException,
      mockArgumentsHost
    );
    expect(mockJson).toBeCalledWith(notFoundExpected);
    mockJson.mockReset();

    const internalErrorExpected = {
      status: 500,
      statusDescription: JSON.stringify({
        statusCode: 500,
        message: 'Internal Server Error'
      }),
      url: 'test'
    };
    allExceptions.catch(
      new InternalServerErrorException,
      mockArgumentsHost
    );
    expect(mockJson).toBeCalledWith(internalErrorExpected);
    mockJson.mockReset();
    done();
  });

});

describe('test 1', () => {
  let appController: AppController;
  let descriptionService: DescriptionService;
  let xlsxService: XlsxService;
  let storageService: StorageService;
  let dataSheetService: DataSheetService;
  let loggerService: LoggerService;
  let allExceptions: AllExceptionsFilter;

  beforeEach(async () => {
    process.env = Object.assign(process.env, { NODE_ENV: 'production' });
    const app: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      controllers: [AppController],
      providers: [StorageService, DescriptionService, XlsxService, DataSheetService, LoggerService, AllExceptionsFilter, {
        provide: APP_FILTER,
        useClass: AllExceptionsFilter,
      },]
    }).compile();

    appController = app.get<AppController>(AppController);
    storageService = app.get<StorageService>(StorageService);
    descriptionService = app.get<DescriptionService>(DescriptionService);
    dataSheetService = app.get<DataSheetService>(DataSheetService);
    xlsxService = app.get<XlsxService>(XlsxService);
    loggerService = app.get<LoggerService>(LoggerService);
    allExceptions = app.get<AllExceptionsFilter>(AllExceptionsFilter);
  });

  it('Exception production', (done) => {
    allExceptions.catch(
      new Error,
      mockArgumentsHost
    );
    expect(process.env.NODE_ENV).toBe('production');
    done();
  });

  it('test', async (done) => {
    jest.spyOn(fs, 'existsSync').mockImplementation(() => {
      return true;
    });
    const file = await xlsxService.writeFile(xlsxData.fileContext, xlsxData.header, [{}]);
    expect(typeof file).toBe('string');
    done();
  })

});



